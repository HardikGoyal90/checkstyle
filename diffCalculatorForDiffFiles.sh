#!/bin/bash  

mkdir -p target/diffForFiles
  
file='target/diffFiles.txt'  
  
i=1  
while read line; do 
  mkdir -p "$(dirname "target/diffForFiles/$line")"
  git diff $CI_MERGE_REQUEST_DIFF_BASE_SHA $CI_COMMIT_SHA -- $line > "target/diffForFiles/$line"
done < $file  
